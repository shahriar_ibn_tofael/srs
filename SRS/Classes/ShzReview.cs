﻿using SRS.Interfaces;
using System;

namespace SRS.Classes
{
    internal class ShzReview 
    {
        private IReview parent;

        public ShzReview(IReview reviewService)
        {
            parent = reviewService;
        }

        public String CreateReview(string userID, string itemID, string reviewComment, int reviewRating)
        {

            return parent.CreateReview(userID, itemID, reviewComment, reviewRating);
            
        }
       
    }
}
