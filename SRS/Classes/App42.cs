﻿using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.review;
using SRS.Interfaces;
using System;

namespace SRS.Classes
{
    public class App42 : IReview
    {
        const string KEY = "bb0c03ab13dff7dfa16586bef2d818d73cbe07c2cb33489626bf8f39a105dc7f";
        const string SECRET = "5f64f3a295ed950d62047667eaeb1ee4c48920a1623278cb6ccf451188ab35aa";

        public App42()
        {
            App42API.Initialize(KEY, SECRET);
            
        }
        public String CreateReview(string userID, string itemID,string reviewComment,int reviewRating)
        {
            App42API.Initialize(KEY, SECRET);
            ReviewService reviewService = App42API.BuildReviewService();
            Review review = reviewService.CreateReview(userID, itemID, reviewComment, reviewRating);
            String jsonResponse = review.ToString();
            return jsonResponse;
        }
        public String GetAllReview()
        {
            return "";
        }
        public String GetAllReviewsCount()
        {
            return "";
        }
        public String GetAllReviews()
        {
            return "";
        }
        public String GetReviewsByItem()
        {
            return "";
        }
        public String GetReviewsCountByItem()
        {
            return "";
        }
        public String GetReviewsByItem(string itemId, double max, double offset)
        {
            return "";
        }
        public string GetHighestReviewByItem(string itemId)
        {
            return "";  
        }
        public string GetLowestReviewByItem(string itemId)
        {
            return "";
        }
        public string GetReviewsCountByItemAndRating(string itemId)
        {
            return "";
        }
        public string AddComment(string itemId)
        {
            return "";
        }
        public string GetCommentsByItem(string itemId)
        {
            return "";
        }
        public string Mute(string itemId)
        {
            return "";
        }
        public string Unmute(string itemId)
        {
            return "";
        }
        public string DeleteReviewByReviewId(string itemId)
        {
            return "";
        }
        public string DeleteCommentByCommentId(string itemId)
        {
            return "";
        }
    }
}
