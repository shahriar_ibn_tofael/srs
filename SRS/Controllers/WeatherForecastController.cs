﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.shephertz.app42.paas.sdk.csharp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SRS.Classes;

namespace SRS.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            //App42API.Initialize(KEY, SECRET);
            var sp = new ShzReview(new App42());
            sp.CreateReview("321222552", "3212321555222", "t5st", 3);

            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
