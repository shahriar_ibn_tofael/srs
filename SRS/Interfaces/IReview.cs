﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRS.Interfaces
{
    interface IReview
    {
        public String CreateReview(string userID, string itemID, string reviewComment, int reviewRating);
    }
}
